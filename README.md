# Installing Minikube

The tutorial shows you how to create your Minikube environment. Minikube provides a simple way of running Kubernetes on your local machine.

## Prerequisite

### Hardware

Based on my experience the minimum hardware requirements of Minikube are:
- vCPU: 2 cores
- RAM: 4 GB

### Virtualisation

Verify CPU virtualization extensions on a Linux:
- vmx – Intel VT-x, virtualization support enabled in BIOS.
- svm – AMD SVM,virtualization enabled in BIOS.

Type the following command to verify that host cpu has support for Intel VT technology, enter:
```bash
grep --color vmx /proc/cpuinfo
```
If the output has the vmx flags, then Intel CPU host is capable of running hardware virtualization.

Type the following command to verify that host cpu has support for AMD–V technology:
```bash
grep --color svm /proc/cpuinfo
```
Again, the output has the svm flags, then AND CPU host is capable of running hardware virtualization.

Verify the CPU architecture information on a Linux:
```bash
lscpu | grep --color Virtualization
```

## Installation

### Update

Before installation update the system:
```bash
sudo apt-get -y update && sudo apt-get -y upgrade && sudo apt-get autoremove && sudo apt-get autoclean && df -h
```

### Permissions

Modify the `/etc/sudoers.d` file:
```bash
echo "`whoami` ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/`whoami`
```
`NOPASSWD` is a tag that means no password will be requested when you switch to `root`.

### Install VirtualBox

Install VirtualBox:
```bash
sudo apt-get -y install virtualbox
```
Add user to `vboxusers` group to be able to create a virtual machine:
```bash
sudo adduser $(whoami) vboxusers
```
Verify the installation:
```bash
sudo VBoxManage -v
```

### Install Kubectl

`kubectl` is a command line interface for running commands against Kubernetes clusters.

Install the latest stable version:
```bash
sudo curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s \
https://storage.googleapis.com/kubernetes-release/release/stable.txt)\
/bin/linux/amd64/kubectl && sudo chmod +x kubectl && sudo mv kubectl /usr/local/bin/
```
Verify the installation:
```bash
sudo kubectl version --output=yaml
```

### Install Minikube

Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your laptop.

Install the latest stable version:
```bash
sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 &&\
sudo mv minikube /usr/local/bin/ &&\
sudo chmod +x /usr/local/bin/minikube
```
Verify the installation:
```bash
minikube version
```

## Start Minikube

Start Minikube executing:
```bash
minikube start --vm-driver=virtualbox
```
This command will download the Minikube ISO (on the first run only), start a Virtualbox VM and create a kubernetes context named 'minikube'. This context contains the configuration to communicate with your minikube cluster. Because Minikube sets this context to default automatically, you can start using your kubernetes cluster right away.

Execute following command to verify the running kubernetes cluster:
```bash
kubectl get services
```
